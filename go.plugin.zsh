install_go() {
    local install_dir="$HOME"
    local go_version="1.18.1"

    # set environment variables
    export GOPATH="${install_dir}/go"
    path+=("${GOPATH}/bin")

    if ! type go 2>&1 >/dev/null; then
        echo "--> Installing go v${go_version} at $install_dir/go"
        echo

        mkdir -p "$install_dir" 2>&1 >/dev/null
        pushd "$install_dir" 2>&1 >/dev/null

        rm -rf "$install_dir/go" \
            && curl -L "https://go.dev/dl/go${go_version}.linux-amd64.tar.gz" \
            | tar -C "$install_dir" -xz

        popd 2>&1 >/dev/null
    fi
}

install_go
